class constantProgress {
  static pending = "pending";
  static on_progress = "on_progress";
  static resolved = "resolved";
}
export default constantProgress;
